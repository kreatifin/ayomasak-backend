<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeNutrition extends Model
{
    protected $table = 'mst_recipe_nutrition';
}
