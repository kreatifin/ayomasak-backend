<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipePhoto extends Model
{
    protected $table = 'mst_recipe_photo';
}
