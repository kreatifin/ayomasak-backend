<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeGuide;
use App\RecipeIngredient;
use App\RecipePrice;
use App\RecipeUtility;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('APITokenJWT');
    }

    public function list(Request $req)
    {
        # code...
    }

    public function new(Request $req)
    {
        $param = $req->all();
        $user = auth()->user();

        if ($param['type'] == 'main') {
            $recipe = new Recipe();
            $recipe->author_id = $user->id;
            $recipe->name = $param['name'];
            $recipe->description = $param['description'];
            $recipe->category_id = 1;
            $recipe->serve_for = $param['serving'];
            $recipe->time_to_cook = $param['cooking_time'];
            $recipe->difficulity = $param['cooking_level'];
            $recipe->save();

            return response()->json(['message' => 'recipe created!']);
        }

        if ($param['type'] == 'ingredient') {
            $recipe = Recipe::find($param['recipe_id']);

            foreach ($param['ingredient'] as $item) {
                $ingredient = new RecipeIngredient();
                $ingredient->recipe_id = $recipe->id;
                $ingredient->name = $item['name'];
                $ingredient->value = $item['value'];
                $ingredient->in_mass = $item['mass'];
                $ingredient->save();
            }

            return response()->json(['message' => 'ingredient saved!']);
        }

        if ($param['type'] == 'guide') {
            $recipe = Recipe::find($param['recipe_id']);

            foreach ($param['guide'] as $item) {
                $guide = new RecipeGuide();
                $guide->recipe_id = $recipe->id;
                $guide->description = $item['text'];
                $guide->save();
            }

            return response()->json(['message' => 'guide saved!']);
        }

        if ($param['type'] == 'utility') {
            $recipe = Recipe::find($param['recipe_id']);

            foreach ($param['utility'] as $item) {
                $utility = new RecipeUtility();
                $utility->recipe_id = $recipe->id;
                $utility->name = $item['util'];
                $utility->save();
            }

            return response()->json(['message' => 'utility saved!']);
        }

        if ($param['type'] == 'price') {
            $recipe = Recipe::find($param['recipe_id']);

            $price = new RecipePrice();
            $price->recipe_id = $recipe->id;
            $price->price = $param['price'];
            $price->min_quantity = $param['min_qty'];
            $price->active = true;
            $price->save();

            return response()->json(['message' => 'price saved!']);
        }
    }

    public function detail(Request $req, $id)
    {
        $recipeDetail = Recipe::with([
            'ingredient:recipe_id,name,value,in_mass', 
            'guide:recipe_id,description',
            'utility:recipe_id,name',
            'nutrition',
            'price:recipe_id,price,min_quantity',
            ])->where('id', $id)->first();

        return response()->json($recipeDetail);
    }

    public function publish(Request $req)
    {
        $recipe = Recipe::where([
            'id' => $req->input('recipe_id'), 
            'publish' => false, 
            ])->first();

        if (is_null($recipe)) {
            return response()->json(['message' => 'recipe not found']);
        }

        $recipe->publish = true;
        $recipe->save();

        return response()->json(['message' => 'recipe updated']);
    }
}
