<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('APITokenJWT', ['except' => ['login', 'register', 'verifyOtp']]);
    }

    public function register(Request $req)
    {
        $param = $req->only('firstName', 'lastName', 'email', 'phone', 'password');

        $checkerEmail = User::where('email', $param['email'])->first();
        if (!is_null($checkerEmail)) {
            return response()->json(['message' => 'email already registered']);
        }
        $checkerPhone = User::where('phone_number', $param['phone'])->first();
        if (!is_null($checkerPhone)) {
            return response()->json(['message' => 'phone already registered']);
        }

        $reg = new User();
        $reg->first_name = $param['firstName'];
        $reg->last_name = $param['lastName'];
        $reg->email = $param['email'];
        $reg->phone_number = $param['phone'];
        $reg->password = Hash::make($param['password']);
        $reg->type = 'user';
        $reg->otp = mt_rand(100000, 999999);
        $reg->save();

        return response()->json($reg);
    }

    public function verifyOtp(Request $req)
    {
        $param = $req->only('otp', 'phone');

        $user = User::where([
            'phone_number' => $param['phone'], 
            'otp' => $param['otp'], 
            'active' => false
            ])->first();

        if (is_null($user)) {
            return response()->json(['message' => 'user not found'], 401);
        }

        $user->otp = null;
        $user->active = true;
        $user->save();

        return response()->json(['message' => 'user activated'], 201);
    }

    public function login(Request $req)
    {
        $param = $req->only('phone', 'password');

        $user = User::where([
            'phone_number' => $param['phone']
            ])->first();

        if (is_null($user)) {
            return response()->json(['message' => 'user not found'], 401);
        }

        if (!$user->active) {
            return response()->json(['message' => 'user not active'], 401);
        }
        
        $param['phone_number'] = $param['phone'];
        unset($param['phone']);
        if (!$token = auth('api')->attempt($param)) {            
            return response()->json(['message' => 'Email atau Password tidak sesuai.'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function profile(Request $req)
    {
        $user = User::find(auth()->user()->id);
        return response()->json($user);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL()
        ]);
    }
}
