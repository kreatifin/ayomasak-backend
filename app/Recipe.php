<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'mst_recipe';

    public function ingredient()
    {
        return $this->hasMany('App\RecipeIngredient');
    }

    public function guide()
    {
        return $this->hasMany('App\RecipeGuide');
    }

    public function utility()
    {
        return $this->hasMany('App\RecipeUtility');
    }

    public function nutrition()
    {
        return $this->hasMany('App\RecipeNutrition');
    }

    public function price()
    {
        return $this->hasOne('App\RecipePrice');
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
