<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeGuide extends Model
{
    protected $table = 'mst_recipe_guide';
}
