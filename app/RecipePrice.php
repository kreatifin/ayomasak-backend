<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipePrice extends Model
{
    protected $table = 'mst_recipe_price';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
