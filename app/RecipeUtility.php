<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeUtility extends Model
{
    protected $table = 'mst_recipe_utility';
}
