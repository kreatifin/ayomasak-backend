<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', ['uses' => 'UserController@register']);
Route::post('verify-otp', ['uses' => 'UserController@verifyOtp']);
Route::post('login', ['uses' => 'UserController@login']);
Route::get('me', ['uses' => 'UserController@profile']);
// Route::post('logout', ['uses' => 'UserController@logout']);

Route::group(['prefix' => 'recipe'], function () {
    Route::post('new', ['uses' => 'RecipeController@new']);
    Route::get('{id}', ['uses' => 'RecipeController@detail']);
    Route::post('publish', ['uses' => 'RecipeController@publish']);
});
