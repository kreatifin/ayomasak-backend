<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_payment', function (Blueprint $table) {
            $table->id();
            $table->integer('trx_recipe_id');
            $table->double('amount');
            $table->boolean('status');
            $table->string('channel', 100);
            $table->string('virtual_account', 100)->nullable();
            $table->string('card_verification', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_payment');
    }
}
