<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxRecipeEngagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_recipe_engagement', function (Blueprint $table) {
            $table->id();
            $table->integer('recipe_id');
            $table->enum('type', ['view', 'share']);
            $table->integer('user_id')->nullable();
            $table->enum('share_platform', ['email', 'facebook', 'instagram', 'twitter', 'link', 'print'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_recipe_engagement');
    }
}
