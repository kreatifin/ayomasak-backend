<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_recipe', function (Blueprint $table) {
            $table->id();
            $table->integer('author_id');
            $table->string('name', 255);
            $table->text('description');
            $table->integer('category_id');
            $table->integer('serve_for');
            $table->string('time_to_cook', 10);
            $table->enum('difficulity', ['low', 'mid', 'high']);
            $table->boolean('publish')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_recipe');
    }
}
